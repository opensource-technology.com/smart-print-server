'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _boxen = require('boxen');

var _boxen2 = _interopRequireDefault(_boxen);

var _printerList = require('./printerList');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

if (process.env.NODE_ENV !== 'production') {
  var dotenv = require('dotenv');
  dotenv.config();
}

var app = (0, _express2.default)();
require('./setup')(app);

var config = {
  hostname: '',
  port: process.env.NODEPRINT_PORT || 5454
};

app.listen(config.port, _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
  return _regenerator2.default.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _printerList.printerList)();

        case 2:
          console.log((0, _boxen2.default)('Node Print V1.0.0', { padding: 1, borderStyle: 'round', borderColor: 'cyan', backgroundColor: 'green' }));
          console.log('Express server listening on ' + config.hostname + ' with port ' + config.port);

        case 4:
        case 'end':
          return _context.stop();
      }
    }
  }, _callee, this);
})));