'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

var _constant = require('../../constant');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var printController = {
  print: function print(req, res) {
    var _req$query = req.query,
        fileName = _req$query.fileName,
        paperType = _req$query.paperType;

    if (!fileName) {
      return res.status(404).json({ error: 'cant file' });
    }
    _model2.default.print(fileName, paperType).then(function (r) {
      return res.status(200).json(r);
    }).catch(function (error) {
      return res.status(500).json({ error: error });
    });
  },
  saveFileAndPrint: function saveFileAndPrint(req, res) {
    var url = req.body.url;
    var paperType = req.query.paperType;


    if (!url) {
      return res.status(404).json({ error: 'cant file' });
    }

    _model2.default.saveFile(url).then(function (r) {
      _model2.default.print(r.fileName, paperType).then(function (r) {
        return res.status(200).json(r);
      }).catch(function (error) {
        return res.status(500).json({ error: error });
      });
    }).catch(function (error) {
      return res.status(500).json({ error: error });
    });
  },
  printByPrinterId: function printByPrinterId(req, res) {
    var printerId = req.params.printerId;
    var fileName = req.query.fileName;

    if (!fileName) {
      return res.status(404).json({ error: 'fileName is require' });
    }
    if (!printerId) {
      return res.status(500).json({ error: 'printer id is require' });
    }
    if (!_constant.constant.printerLists && !_constant.constant.printerLists.data) {
      return res.status(500).json({ error: 'no have printer list' });
    }

    _model2.default.printByPrinterId(printerId, fileName).then(function (r) {
      if (r.code === 404) {
        return res.status(404).json(r);
      }
      return res.status(200).json(r);
    }).catch(function (error) {
      return res.status(500).json({ error: error });
    });
  },
  saveFile: function saveFile(req, res) {
    var url = req.body.url;
    if (!url) {
      return res.status(404).json({ error: 'cant file' });
    }

    _model2.default.saveFile(url).then(function (r) {
      console.log(r);
      return res.status(200).json(r);
    }).catch(function (error) {
      return res.status(500).json({ error: error });
    });
  }
};

exports.default = printController;