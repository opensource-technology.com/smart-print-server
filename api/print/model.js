'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _constant = require('./../../constant');

var _function = require('./../../function');

var _printFunction = require('./../../printFunction');

var _printFunction2 = _interopRequireDefault(_printFunction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var Model = {
  print: function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(fileName) {
      var paperType = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var option = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var pathAndFileName;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              pathAndFileName = _constant.constant.pathName + fileName;

              if (paperType) {
                if (process.env[paperType] !== undefined) {
                  option.printer = process.env[paperType];
                }
              }
              _context.next = 4;
              return _function.fn.sleep(5000);

            case 4:
              _context.prev = 4;
              _context.next = 7;
              return _printFunction2.default.printFile(pathAndFileName, option);

            case 7:
              return _context.abrupt('return', { status: 'ok' });

            case 10:
              _context.prev = 10;
              _context.t0 = _context['catch'](4);
              throw _context.t0.toString();

            case 13:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this, [[4, 10]]);
    }));

    function print(_x3) {
      return _ref.apply(this, arguments);
    }

    return print;
  }(),
  printByPrinterId: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee2(printerId, fileName) {
      var printerAllow, printerName, printed;
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              printerAllow = _constant.constant.printerLists.data.filter(function (value) {
                return +value.id === +printerId;
              });

              if (printerAllow.length) {
                _context2.next = 3;
                break;
              }

              return _context2.abrupt('return', { error: 'printer not found', code: 404 });

            case 3:
              printerName = printerAllow[0].name;
              _context2.prev = 4;
              _context2.next = 7;
              return Model.print(fileName, '', {
                printer: printerName
              });

            case 7:
              printed = _context2.sent;
              return _context2.abrupt('return', printed);

            case 11:
              _context2.prev = 11;
              _context2.t0 = _context2['catch'](4);
              throw _context2.t0.toString();

            case 14:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, this, [[4, 11]]);
    }));

    function printByPrinterId(_x4, _x5) {
      return _ref2.apply(this, arguments);
    }

    return printByPrinterId;
  }(),
  saveFile: function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee4(url) {
      var _this = this;

      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt('return', _printFunction2.default.writeSaveFile(url).then(function () {
                var _ref4 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee3(r) {
                  return _regenerator2.default.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          return _context3.abrupt('return', { fileName: r.fileName });

                        case 1:
                        case 'end':
                          return _context3.stop();
                      }
                    }
                  }, _callee3, _this);
                }));

                return function (_x7) {
                  return _ref4.apply(this, arguments);
                };
              }()).catch(function (error) {
                throw error.toString();
              }));

            case 1:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    function saveFile(_x6) {
      return _ref3.apply(this, arguments);
    }

    return saveFile;
  }()
};
exports.default = Model;