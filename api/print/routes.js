'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setup = setup;

var _controller = require('./controller');

var _controller2 = _interopRequireDefault(_controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function setup(router) {
  router.get('/', _controller2.default.print).get('/:printerId', _controller2.default.printByPrinterId).post('/', _controller2.default.saveFileAndPrint).post('/saveFile', _controller2.default.saveFile);
}