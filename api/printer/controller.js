'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _model = require('./model');

var _model2 = _interopRequireDefault(_model);

var _constant = require('./../../constant');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var printerController = {
  printers: function printers(req, res) {
    return res.status(200).json(_constant.constant.printerLists);
  }
};

exports.default = printerController;