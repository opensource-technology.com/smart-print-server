'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _model = require('./model');

Object.defineProperty(exports, 'printerAPI', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_model).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }