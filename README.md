# Installation
- RUN  `$ git clone https://gitlab.opensource-technology.com/smart-hospital-implement/smart-print-server.git`

- Run  `$ npm install`  

- Run  `$ npm install -g pm2`

### Config Environment Variables file run-smart-print-server.json

```json
{
    "apps": [
        {
            "name": "smart-print-server",
            "script": "app.js",
            "env": {
                "NODE_ENV": "production",
                "NODEPRINT_PORT": "5454",
                "NODEPRINT_PAPER_A5": "Microsoft Print to PDF",
                "NODEPRINT_PAPER_A3": "Microsoft Print to PDF",
                "NODEPRINT_PAPER_THERMAL": "Microsoft Print to PDF"
            }
        }
    ]
}

```  

- Run  `$ pm2 start run-smart-print-server.json`

#### GET Printer list connected

[http://127.0.0.1:5454/api/printer](http://127.0.0.1:5454/api/printer)

#### GET Test print
[http://127.0.0.1:5454/api/print?fileName=test_print.pdf&paperType=NODEPRINT_PAPER_THERMAL](http://127.0.0.1:5454/api/print?fileName=file_test.pdf&paperType=NODEPRINT_PAPER_THERMAL)

## Document 
[smart-print-server]( https://documenter.getpostman.com/view/6274377/SztJz4Af?version=latest )