'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.printerList = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _pdfToPrinter = require('pdf-to-printer');

var _pdfToPrinter2 = _interopRequireDefault(_pdfToPrinter);

var _constant = require('./constant');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var printerList = exports.printerList = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    var printers, objPrinter;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _pdfToPrinter2.default.list();

          case 3:
            printers = _context.sent;
            objPrinter = [];


            if (!printers.length) {
              _constant.constant.printerLists = { data: {}, message: 'Printer not found'
                // return { data: {}, message: 'Printer not found' }
                // return res.status(200).json({ data: {}, message: 'Printer not found' })
              };
            }

            printers.forEach(function (val, index) {
              objPrinter.push({
                id: index,
                name: val
              });
            });
            _constant.constant.printerLists = { data: objPrinter
              // return { data: objPrinter }
              // return res.json({ data: objPrinter })
            };_context.next = 13;
            break;

          case 10:
            _context.prev = 10;
            _context.t0 = _context['catch'](0);
            throw _context.t0;

          case 13:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 10]]);
  }));

  return function printerList() {
    return _ref.apply(this, arguments);
  };
}();