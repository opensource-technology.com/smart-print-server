'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _pdfToPrinter = require('pdf-to-printer');

var _pdfToPrinter2 = _interopRequireDefault(_pdfToPrinter);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _nodeFetch = require('node-fetch');

var _nodeFetch2 = _interopRequireDefault(_nodeFetch);

var _function = require('./function');

var _constant = require('./constant');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var printfn = {
  printFile: function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(pdfUrl) {
      var option = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              return _context.abrupt('return', _pdfToPrinter2.default.print(pdfUrl, option).then(function (r) {
                return r;
              }).catch(function (err) {
                throw Error(err);
              }).finally(function () {
                _function.fn.sleep(5000);
                printfn.removeFile(pdfUrl);
              }));

            case 1:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function printFile(_x2) {
      return _ref.apply(this, arguments);
    }

    return printFile;
  }(),
  writeSaveFile: function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee3(url) {
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt('return', new Promise(function () {
                var _ref3 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee2(resolve, reject) {
                  var fileName, pathAndFileName, response, buffer, pdfPath;
                  return _regenerator2.default.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          if (!_fs2.default.existsSync(_constant.constant.dir)) {
                            _fs2.default.mkdirSync(_constant.constant.dir);
                          }

                          fileName = Date.now().toString() + '.pdf';
                          pathAndFileName = _constant.constant.pathName + fileName;
                          _context2.next = 5;
                          return (0, _nodeFetch2.default)(url);

                        case 5:
                          response = _context2.sent;
                          _context2.next = 8;
                          return response.buffer();

                        case 8:
                          buffer = _context2.sent;
                          pdfPath = _path2.default.join(__dirname, pathAndFileName);

                          _fs2.default.writeFileSync(pdfPath, buffer, 'binary');
                          console.log('===SAVE FILE===');
                          resolve({
                            pdfPath: pdfPath,
                            pathName: _constant.constant.pathName,
                            fileName: fileName,
                            pathAndFileName: pathAndFileName
                          });

                        case 13:
                        case 'end':
                          return _context2.stop();
                      }
                    }
                  }, _callee2, undefined);
                }));

                return function (_x4, _x5) {
                  return _ref3.apply(this, arguments);
                };
              }()));

            case 1:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined);
    }));

    function writeSaveFile(_x3) {
      return _ref2.apply(this, arguments);
    }

    return writeSaveFile;
  }(),
  removeFile: function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee5(fileName) {
      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              return _context5.abrupt('return', new Promise(function () {
                var _ref5 = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee4(resolve, reject) {
                  return _regenerator2.default.wrap(function _callee4$(_context4) {
                    while (1) {
                      switch (_context4.prev = _context4.next) {
                        case 0:
                          _fs2.default.unlink(_path2.default.join(__dirname, fileName), function (err) {
                            if (err) throw err;
                            console.log('===RM FILE===');
                            resolve(true);
                          });

                        case 1:
                        case 'end':
                          return _context4.stop();
                      }
                    }
                  }, _callee4, undefined);
                }));

                return function (_x7, _x8) {
                  return _ref5.apply(this, arguments);
                };
              }()));

            case 1:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, undefined);
    }));

    function removeFile(_x6) {
      return _ref4.apply(this, arguments);
    }

    return removeFile;
  }()
};

exports.default = printfn;