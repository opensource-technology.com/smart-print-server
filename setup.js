'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setupRoutes = function setupRoutes(app) {
  var APP_DIR = __dirname + '/api';
  var features = _fs2.default.readdirSync(APP_DIR).filter(function (file) {
    return _fs2.default.statSync(APP_DIR + '/' + file).isDirectory();
  });
  features.forEach(function (feature) {
    var router = _express2.default.Router();
    var routes = require(APP_DIR + '/' + feature + '/routes');
    routes.setup(router);
    app.use('/api/' + feature, router);
  });
};

module.exports = function (app, config) {
  app.use((0, _cors2.default)());
  app.use(_bodyParser2.default.urlencoded({ extended: true }));
  app.use(_bodyParser2.default.json());
  setupRoutes(app);
};